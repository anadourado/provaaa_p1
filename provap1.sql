CREATE TABLE winemag_data_130k(
cod_winemag SERIAL PRIMARY KEY,
country VARCHAR(50),
description VARCHAR(200),
points FLOAT,
price FLOAT
);



--Calcular o preço médio dos vinhos de cada país utilizando um cursor não vinculado.


DO $$
DECLARE
    -- 1. Declarando o cursor 
	cur_leitura REFCURSOR;
	tupla RECORD;
	media NUMERIC(10,2);
BEGIN
	-- 2. Abrindo o cursor  
	OPEN cur_leitura FOR  
    		SELECT country, count(country) as quantidade, sum(price) as preco 
      		FROM winemag_data_130k
    		WHERE country is NOT null
       		AND price is NOT NULL 
     GROUP by country, price;
	
	LOOP
		FETCH FROM cur_leitura INTO tupla;
		EXIT WHEN NOT FOUND;
        media = tupla.preco / tupla.quantidade;   
        RAISE NOTICE '%, %', tupla.country, media; 
	END LOOP;
	-- 4. Fechando cursor
	CLOSE cur_leitura;
END;
$$

-- Cursor vinculado obtenção de descrição mais longa por país

DO $$
DECLARE
    cur_wines CURSOR FOR
        SELECT country, description
        FROM winemag_data_130k;
    resultado TEXT DEFAULT '';
    tupla RECORD;
BEGIN
    OPEN cur_wines
    FETCH cur_wines INTO tupla;
    WHILE FOUND LOOP
        resultado := resultado || tupla.country || ': ' || tupla.description || ', ';
        FETCH cur_wines INTO tupla;
    END LOOP;
    CLOSE cur_wines;
    RAISE NOTICE '%', resultado;
END;
$$


--sOsresultadosobtidospeloscursoresdevemserarmazenadosnumatabelacomosseguintescampos-id(chaveprimária,autoincremento)-nome_pais-preco_medio-descricao_mais_longaMensagemdecommit:feat(tabela-resultante):tabelaparaarmazenamentodosresultados

CREATE TABLE resultado_cursor (     id SERIAL PRIMARY KEY,     nome_pais VARCHAR(50),     preco_medio FLOAT,     descricao_mais_longa VARCHAR(200) );

